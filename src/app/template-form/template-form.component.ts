import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: [
    './template-form.component.css',
    '../app.component.css'
  ]
})
export class TemplateFormComponent implements OnInit {

  protected form;

  /*Para iniciarmos os valores no template 
  faz-se uso do property-binding, ligando o 
  elemento do template que se dejesa inicializar
  com o atributo que irá fornecer os valores.  */
  protected user:any = {
    name:null,
    email:null
  };

  constructor(private http:Http) { }

  ngOnInit() {
  }

  public submitForm(form:NgForm):void{
    
    this.http.post(`https://httpbin.org/post`,JSON.stringify(form.value))
    .map(data => data)
    .subscribe(data=>{
      console.log(data);
    });
    
    console.log(form.value);
  }


  protected verifyInvalidAndTouched(field):boolean{
    return field.invalid && field.touched;
  }

  protected addInvalidValueMessage(field):any{
    return {'has-error': this.verifyInvalidAndTouched(field)};
  }


  protected fechtAdressByCep(cep, form:NgForm){
  
    let cepSanatize;
    let cepValidatorRegex = /^[0-9]{8}$/;
    
    console.log(form);

    if(cep){
      cepSanatize = cep.toString().replace(/\D/g, '');
      if(cepValidatorRegex.test(cepSanatize))
        this.createAdress(cepSanatize, form);
      else throw new Error("CEP Inválido!");
    } else throw new Error("CEP Inválido!");

  }

  private createAdress(cep:string, form:NgForm){
    this.http.get(`https://viacep.com.br/ws/${cep}/json`)
    .map(data => data.json())
    .subscribe(data=>{
      console.log(data);

      /*form.setValue({
        name: form.value.name,
        email: form.value.email,
        adress: {
          street: data.logradouro,
          cep: data.cep,
          number: '',
          compl: '',
          district: data.bairro,
          city: data.localidade,
          state: data.uf
        }
      });*/

      form.form.patchValue({
        adress: {
          street: data.logradouro,
          cep: data.cep,
          district: data.bairro,
          city: data.localidade,
          state: data.uf
        }
      });

    });
  }

}