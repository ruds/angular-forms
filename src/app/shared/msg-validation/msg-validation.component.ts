import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-msg-validation',
  templateUrl: './msg-validation.component.html',
  styleUrls: ['./msg-validation.component.css']
})
export class MsgValidationComponent implements OnInit {

@Input('showMsg') showMsg:boolean = false;
@Input('msgText') msgText:string = '';

  constructor() { }

  ngOnInit() {
  }

}
