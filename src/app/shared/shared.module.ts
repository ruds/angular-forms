import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';

import { MsgValidationComponent } from "./msg-validation/msg-validation.component";
import { DropdownService } from './services/dropdown.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
  ],
  declarations: [MsgValidationComponent],
  exports:[
    MsgValidationComponent,
    FormsModule,
    HttpModule,
    CommonModule,
  ],
  providers:[
    DropdownService
  ]
})
export class SharedModule { }