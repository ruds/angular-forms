import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class DropdownService {

  constructor(private http:Http) { }

  public getBRStates(){
    return this.http.get('assets/data/BRStates.json')
    .map((resp:Response)=>resp.json());
  }
}
