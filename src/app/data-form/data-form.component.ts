import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { DropdownService } from '../shared/services/dropdown.service';
import { StatesBR } from '../shared/models/states-br';

@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: [
    './data-form.component.css',
    '../app.component.css'
  ]
})
export class DataFormComponent implements OnInit {

  /*Nos formularios data-driven todos o código 
  relacionado aos dados existentes no form
  ficam no componente. No código HTML ficará
  somente a estrutura da página. */
  protected myForm:FormGroup;

  protected statesBR:Array<StatesBR>;

  //#1 - FormBuilder sendo injeto no construtor
  constructor(
    private formBuilder:FormBuilder,
    private http:Http,
    private dropDownService:DropdownService
  ) { }

  ngOnInit() {


    this.dropDownService.getBRStates()
    .subscribe(data => {
      this.statesBR = data;
      console.log(this.statesBR);
    });

    /*Para iniciar o formulário instancia-se um
    objeto do tipo FormGroup passando como parâmetro
    para o seu construtor um objeto que contém
    os atributos que se deseja que estejam presentes
    no formulário. 
    Cada atributo do objeto que representa o 
    o formulário deverá ser do tipo FormControl(). 
    Caso se deseje que o campo tenha um valor 
    inicial é possível passar esse valor como 
    parâmetro para o construtor de FormControl(),
    caso não se deseje, passa-se null.*/
    
    //VERBOSE MODE
    /*this.myForm = new FormGroup({
      nome: new FormControl(null),
      email: new FormControl(null),
    });*/

    //FormBuilder MODE
    /*
    Outra maneira de iniciar um formulário reativo é
    fazendo uso do construtor de formulário do próprio
    Angular, o FormBuilder. 
    
    Para utiliza-lo, injetamos ele no construtor 
    da classe[#1].
    Para usa-lo de forma simplificada faremos uso do
    método "group()" do FormBuilder que retorna um
    objeto do tipo FormGroup e que recebe como parâmetro
    um objeto cujo atributos representam os elementos
    do formulário que esta sendo criado[#2]. Cada atributo
    desse objeto pode receber um array contendo os valores
    iniciais e outras configurações desejadas.[#3]
    */


    /*Para validação utilizando data-driven,
    são passados os validadores para os campos
    dentro do array usados para definir os
    atributos. Pode-se passar um array contendo
    as validações ou apenas o pŕoprio validador
    no caso de somente um ser necessário.
    Por padrão, será passando sempre dentro de
    um array. */

    //[#2] Utilizando método group() de FormGroup
    this.myForm = this.formBuilder.group({

      //#3 Exemplo de atributo recebendo array com conf inicial
      name:[null,[Validators.required]],
      email:[null, [Validators.required, Validators.email]],
      adress: this.formBuilder.group(
        {
          street: [null,[Validators.required]],
          cep: [null,[Validators.required]],
          number: [null,[Validators.required]],
          compl: [null],
          district: [null,[Validators.required]],
          city: [null,[Validators.required]],
          state: [null,[Validators.required]]
        }
      ) 
    });

  }

  protected submitForm():void{
    
    if(this.myForm.valid){
      console.log("Válido!",this.myForm.value);
    }
    else{
      console.log("Inválido!");
      this.verifyValidation(this.myForm);
    }

  }

  protected resetForm():void{
    this.myForm.reset();
  }

  protected verifyInvalidAndTouched(fieldName:string):boolean{
    let field:any = this.myForm.get(fieldName);
    return field.invalid && 
    (field.touched || field.dirty);
  }

  protected verifyInvalidEmail(){
    let emailField = this.myForm.get('email');
    if(emailField.errors){
      return emailField.errors['email']
        && emailField.touched;
    }
  }

  protected addInvalidValueMessage(fieldName:string):any{
    return {'has-error': this.verifyInvalidAndTouched(fieldName)};
  }


  protected fechtAdressByCep(){
  
    let cep:string = this.myForm.get('adress.cep').value;
    let cepSanatize;
    let cepValidatorRegex = /^[0-9]{8}$/;
    
    console.log(this.myForm);

    if(cep){
      cepSanatize = cep.toString().replace(/\D/g, '');
      if(cepValidatorRegex.test(cepSanatize))
        this.createAdress(cepSanatize);
      else throw new Error("CEP Inválido!");
    } else throw new Error("CEP Inválido!");

  }

  private createAdress(cep:string,){
    this.http.get(`https://viacep.com.br/ws/${cep}/json`)
    .map(data => data.json())
    .subscribe(data=>{

      console.log(data);

      this.myForm.patchValue({
        adress: {
          street: data.logradouro,
          cep: data.cep,
          district: data.bairro,
          city: data.localidade,
          state: data.uf
        }
      });

      this.myForm.get('adress.number').setValue(5);

    });
  }

  protected verifyValidation(group:FormGroup):void{
    
    Object.keys(group.controls).forEach(field =>{
      const control = group.get(field);
      control.markAsDirty();
      if(control instanceof FormGroup)
        this.verifyValidation(control);
    });    
  }

}